import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Single from "./pages/Single";
import SinglePerson from "./pages/SinglePerson";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" index element={<Home />} />
        <Route path="/single/:id" element={<Single />} />
        <Route path="/single-person/:id" element={<SinglePerson />} />
      </Routes>
    </>
  );
}

export default App;
