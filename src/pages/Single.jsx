import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import api from "../api";

function Single() {
  const params = useParams();
  const [show, setShow] = useState(null);
  useEffect(() => {
    const controller = new AbortController();
    api
      .get(`/shows/${params.id}`, {
        params: { embed: ["cast", "episodes"] },
        signal: controller.signal,
      })
      .then((res) => {
        setShow(res.data);
      })
      .catch((err) => {
        console.log(err.response);
      });
    return () => {
      controller.abort();
    };
  }, [params]);
  return (
    <div className="p-3 space-y-5">
      <h4 className="text-lg font-bold shadow">Title: {show?.name}</h4>
      <p className="text-light text-gray-900">
        Genre(s): {show?.genres?.join(", ")}
      </p>
      <p className="text-light text-gray-900">
        Country: {show?.network?.country?.name}
      </p>
      <p className="text-light text-gray-900">
        Airdate: {show?.schedule?.time} - {show?.schedule?.days?.join(", ")}
      </p>
      <img
        className="rounded m-4 shadow"
        src={show?.image?.medium}
        alt={show?.name}
      />
      <hr />
      <div className="p-2">
        Cast:
        <ul className="ml-4">
          {show?._embedded?.cast?.map((em) => (
            <li key={em.person.id}>
              <Link
                className="text-black-400 underline"
                to={`/single-person/${em.person.id}`}
              >
                {em.person.name}
              </Link>
            </li>
          ))}
        </ul>
      </div>
      <p>
        <span dangerouslySetInnerHTML={{ __html: show?.summary }}></span>
      </p>
      <div className="border-[1px]">
        <h3 className="text-center text-lg">Episodes</h3>
        <div className="grid  sm:grid-cols-2 lg:grid-cols-4 gap-4">
          {show?._embedded?.episodes?.map((em) => (
            <div className="border-2 shadow p-2 rounded  mx-auto" key={em.id}>
              <Link className="" to="/">
                <img
                  className="rounded"
                  alt={em.name}
                  src={em?.image?.medium}
                />
                <div className="text-center mt-2 text-gray-500">
                  {em.name} - {em.airdate}
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Single;
