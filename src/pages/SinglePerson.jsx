import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import api from "../api";

function SinglePerson() {
  const params = useParams();
  const [person, setPerson] = useState(null);
  useEffect(() => {
    const controller = new AbortController();
    api
      .get(`/people/${params.id}`, {
        signal: controller.signal,
      })
      .then((res) => {
        console.log(res);
        setPerson(res.data);
      })
      .catch((err) => {
        console.log(err.response);
      });
    return () => {
      controller.abort();
    };
  }, [params]);
  return (
    <div className="m-4">
      <h3 className="text-gray-500 font-bold text-lg">{person?.name}</h3>
      <img
        src={person?.image?.medium}
        className="shadow rounded"
        alt={person?.name}
      />
      <p> Date of Birth: {person?.birthday}</p>
      <p> Country: {person?.country?.name}</p>
    </div>
  );
}

export default SinglePerson;
