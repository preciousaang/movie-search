import { Field, Form, Formik } from "formik";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import api from "../api";

function Home() {
  const [results, setResults] = useState([]);
  const onSearch = (values, actions) => {
    api
      .get("/search/shows", { params: { q: values.query } })
      .then((res) => {
        console.log(res);
        setResults(res.data);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };
  return (
    <>
      <h2 className="text-lg font-bold mb-10">Movie Search</h2>
      <Formik onSubmit={onSearch} initialValues={{ query: "" }}>
        <Form>
          <Field
            className="border-2 border-gray-400 rounded-md outline-none m-4"
            name="query"
          />
          <button className="border-[1px] border-gray-600 p-2" type="sumit">
            Search
          </button>
        </Form>
      </Formik>
      <div className="w-full m-4 rounded border-2 border-gray-400 p-2">
        <h2 className="text-lg">Movie list</h2>
        <hr />

        <div className="grid grid-cols-3 ">
          {results.map(({ show }) => (
            <div key={show.id} className="m-2 space-y-2">
              <img
                src={show?.image?.medium}
                className="mx-auto rounded"
                alt={show.name}
              />
              <p className="text-center">
                <Link to={`/single/${show?.id}`}>{show.name}</Link>
              </p>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

export default Home;
