import axios from "axios";

const api = axios.create({
  baseURL: "https://api.tvmaze.com",
  headers: {
    Accept: "application/json",
  },
});

export default api;
